package Att1;

import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Scanner;

public class Test {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        File fileInput = new File(scanner.nextLine());
        File fileOutput = new File("src/main/java/att1/TestOutput.txt");
        FileService fileService = new FileService();
        fileService.readFile(fileInput, fileOutput);
    }
}

