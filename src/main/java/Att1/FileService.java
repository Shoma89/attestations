package Att1;

import java.io.*;
import java.util.*;

public class FileService {
    public void readFile(File fileInput, File fileOutput){
        String[] arr;
        ArrayList<String> arrayList = new ArrayList<>();
        ArrayList<String> arrayList1 = new ArrayList<>();
        HashMap<String, Integer> map = new HashMap<>();
        try(BufferedReader reader = new BufferedReader(new FileReader(fileInput))) {
            String line;
            while ((line = reader.readLine()) != null){
                for(String tempString : line.split(" ")) {
                    String tempString2 = tempString.toLowerCase().replaceAll("\\p{P}", "");
                    arrayList.add(tempString2);
                    map.put(tempString2, 0);
                }
            }
            writeFile(fileOutput, arrayList, map);
            map.clear();
            for (String tempString : arrayList){
                arr = (tempString.split("(?!^)"));
                for (int i = 0; i < arr.length; i++){
                    arrayList1.add(arr[i]);
                    map.put(arr[i], 0);
                }
            }
            writeFile(fileOutput, arrayList1, map);
        } catch (IOException e){
            throw new RuntimeException();
        }
    }
    public static void writeFile(File fileOutput, ArrayList<String> list, Map<String, Integer> map){
        Collections.sort(list);
        Map<String, Integer> treeMap = new TreeMap<>(map);
        try (BufferedWriter writer = new BufferedWriter(new FileWriter(fileOutput, true))){
            for(Map.Entry<String, Integer> entry : treeMap.entrySet()) {
                for (String tempString : list) {
                    if (tempString.equals(entry.getKey()))
                        treeMap.put(tempString, treeMap.get(tempString) + 1);
                }
                writer.write((entry.getKey()) + " - " + entry.getValue() + "\n");
            }
            writer.write("***********************************" + "\n");
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
